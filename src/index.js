/**
 * This function perform a left rotation operation on an array
 * @param {array} a array to rotate
 * @param {number} d the number of rotations
 */
function rotLeft(a, d) {
  const restLength = -(a.length - d);
  const rotArray = a.slice(0, d);
  const restArray = a.slice(restLength);
  return restArray.concat(rotArray);
}

module.exports = rotLeft;
